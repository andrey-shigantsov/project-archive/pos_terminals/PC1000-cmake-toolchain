if(_TOOLCHAIN_POS_PC1000_ELFTOBIN_LOADED)
    return()
endif()
set(_TOOLCHAIN_POS_PC1000_ELFTOBIN_LOADED 1)

macro(add_elftobin_target TARGET BIN_FILE)
add_custom_command(OUTPUT ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${BIN_FILE}
  COMMAND "${CMAKE_COMMAND}" -E chdir ${CMAKE_RUNTIME_OUTPUT_DIRECTORY} elftobin ${TARGET} ${BIN_FILE} PC1000---APP
  DEPENDS ${TARGET}
)
add_custom_target(${BIN_FILE} ALL
  DEPENDS
    ${TARGET}
    ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${BIN_FILE}
)
add_dependencies(${BIN_FILE} ${TARGET})
set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES 
  ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${BIN_FILE}
)
endmacro()
